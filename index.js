/**
 * Created by DICH on 2/17/2017.
 */
var _defaults = require('lodash').defaults;
var remoteMethodBoot = require('./src/remote_method_boot');
var path = require('path');

module.exports = function (lbApp, options) {
    options = _defaults({}, options, {
        mountPath: '/remote-api',
        remoteConfigFilePath: './remote/remote.config.js',
        services: {}
    });

    remoteMethodBoot(lbApp, options);

    lbApp.use(options.mountPath, routes(lbApp, options))
}

function routes(lbApp, options) {
    var loopback = lbApp.loopback;
    var router = new loopback.Router();

    router.post('/', function (req, res) {
        res.send({
            apis: lbApp.remoteApis,
            configs: lbApp.remoteConfigs
        })
    })

    router.use(loopback.static(path.join(__dirname, 'public')));

    return router;
}