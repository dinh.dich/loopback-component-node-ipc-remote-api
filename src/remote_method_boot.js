/**
 * Created by DICH on 7/9/2016.
 */
var nodeIpcService = require('node-ipc-service');
var IPCServer = nodeIpcService.IPCServer;
var path = require('path');

var reqCountIndex = -1;
var requestTimeMark = [];

module.exports = function (application, options) {
    application.remoteApis = {};
    application.remoteConfigs = {};

    var remoteConfig = require(path.resolve(options.remoteConfigFilePath));

    var handleMethodConfig = function (modelName, methodConfigInfo) {
        methodConfigInfo.exports = methodConfigInfo.exports || {};
        methodConfigInfo.excludes = methodConfigInfo.excludes || [];

        if (methodConfigInfo.path) {
            var model = require(methodConfigInfo.path);
            for (let field in model) {
                if (typeof model[field] == 'function') {
                    var isExport = false;
                    if (methodConfigInfo.exportAll ||
                        (methodConfigInfo.exports[field] && methodConfigInfo.excludes.indexOf(field) == -1)) {
                        isExport = true;
                    }

                    if (isExport) {
                        let methodName = methodConfigInfo.exports[field] && methodConfigInfo.exports[field].alias ?
                            methodConfigInfo.exports[field].alias : field;
                        addMethod(application, id, server, model, modelName, methodName, model[field], methodConfigInfo.exports[field] || {});
                    }
                }
            }
        }

        if (methodConfigInfo.object) {
            model = methodConfigInfo.object;

            methodConfigInfo.exports = methodConfigInfo.exports || {}

            for(let field in methodConfigInfo.exports) {
                let methodName = methodConfigInfo.exports[field] && methodConfigInfo.exports[field].alias ?
                    methodConfigInfo.exports[field].alias : field;
                addMethod(application, id, server, model, modelName, methodName, model[field], methodConfigInfo.exports[field] || {});
            }

            // for (let field in model) {
            //     if (typeof model[field] == 'function') {
            //         isExport = false;
            //         if (methodConfigInfo.exportAll ||
            //             (methodConfigInfo.exports[field] && methodConfigInfo.excludes.indexOf(field) == -1)) {
            //             isExport = true;
            //         }
            //
            //         if (isExport) {
            //             let methodName = methodConfigInfo.exports[field] && methodConfigInfo.exports[field].alias ?
            //                 methodConfigInfo.exports[field].alias : field;
            //             addMethod(application, id, server, model, modelName, methodName, model[field], methodConfigInfo.exports[field] || {});
            //         }
            //     }
            // }
        }
    }

    for (var id in remoteConfig) {
        var remoteInfo = remoteConfig[id];

        options.services[id] = options.services[id] || {};
        remoteInfo.host = options.services[id].host || remoteInfo.host;
        remoteInfo.domain = options.services[id].domain || remoteInfo.domain;
        remoteInfo.port = options.services[id].port || remoteInfo.port;
        remoteInfo.secretKey = options.services[id].secretKey || remoteInfo.secretKey;

        if (!application.remoteConfigs[id]) {
            application.remoteConfigs[id] = {
                host: remoteInfo.host,
                domain: remoteInfo.domain,
                port: remoteInfo.port
            };
        }
        if(!application.remoteApis[id]) {
            application.remoteApis[id] = {};
        }

        var secretKey = remoteInfo.secretKey || options.secretKey;
        var server = new IPCServer(id, remoteInfo.host, remoteInfo.port, secretKey);
        server.on('started', function (id) {
            console.log('started ' + id);
        })

        // var remoteMethodConfig = require(remoteInfo.method_config);
        var remoteMethodConfig = remoteInfo.remotes;
        for (var modelName in remoteMethodConfig) {
            var methodConfigInfo = remoteMethodConfig[modelName];

            if (!application.remoteApis[id][modelName]) {
                application.remoteApis[id][modelName] = {
                    description: methodConfigInfo.description || ''
                };
            }

            if(!methodConfigInfo.methods || methodConfigInfo.methods.length == 0) {
                handleMethodConfig(modelName, methodConfigInfo);
            } else {
                for(var i = 0; i < methodConfigInfo.methods.length; i ++) {
                    handleMethodConfig(modelName, methodConfigInfo.methods[i]);
                }
            }
        }

        server.start();
    }
}

var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
    var fnStr = func.toString().replace(STRIP_COMMENTS, '');
    var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')'))/*.match(ARGUMENT_NAMES)*/;
    result = result.split(',');
    for (var i = 0; i < result.length; i++) {
        result[i] = result[i].trim()
    }
    if (result === null)
        result = [];
    return result;
}

function addMethod(application, serviceId, server, object, modelName, method, func, funcInfo) {
    server.addMethod(modelName + '.' + method, function (params, callback) {
        params.push(function () {
            callback(null, Array.from(arguments));
        });

        func.apply(object, params);

        //count request
        reqCountIndex = (reqCountIndex + 1) % 5000;
        requestTimeMark[reqCountIndex] = new Date().getTime();
    });

    funcInfo = funcInfo || {};

    if (!application.remoteApis[serviceId][modelName].methods) {
        application.remoteApis[serviceId][modelName].methods = [];
    }

    application.remoteApis[serviceId][modelName].methods.push({
        name: method,
        description: funcInfo.description || '',
        params: funcInfo.params || getParamNames(func),
        returns: funcInfo.returns || []
    })
};
